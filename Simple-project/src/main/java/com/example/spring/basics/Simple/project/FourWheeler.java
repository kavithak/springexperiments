package com.example.spring.basics.Simple.project;

import org.springframework.stereotype.Component;

@Component
public class FourWheeler implements VehicleFeatures {

	@Override
	public int numberOfWheels() {
		
		return 4;
	}

}
