package com.example.spring.basics.Simple.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FeautresInterfaceImpl {

	@Autowired
	private VehicleFeatures vehicleFeatures;
	
	public FeautresInterfaceImpl(VehicleFeatures vehicleFeatures) {
		this.vehicleFeatures = vehicleFeatures;
	}
	
	public int getNumberofWheels() {
		return vehicleFeatures.numberOfWheels();
	}
}
