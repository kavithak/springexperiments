package com.example.spring.basics.Simple.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SimpleProjectApplication {

	public static void main(String[] args) {
		
		
		ApplicationContext applicationContext = SpringApplication.run(SimpleProjectApplication.class, args);
		FeautresInterfaceImpl bean = applicationContext.getBean(FeautresInterfaceImpl.class);
		System.out.println(bean.getNumberofWheels());
	}

}
