package com.example.spring.basics.Simple.project;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class TwoWheeler implements VehicleFeatures {

	@Override
	public int numberOfWheels() {
		return 2;
	}

}
